import { createRouter, createWebHistory } from 'vue-router'

import Search from '../pages/blog/search.vue'
import Index from '../pages/blog/index.vue'
import New from '../pages/blog/new.vue'

import Detail from '../pages/blog/_id.vue'
const routes = [{
    path: '/',
    component: Index
    },
    { 
        path: '/edit/:id',
        component: Detail
        
    },
    {
        path: '/search',
        component: Search
        
    },
    {
        path: '/New',
        component: New
        
    },
   
]

const router = createRouter({
    history: createWebHistory(),
    routes: routes
})

export default router