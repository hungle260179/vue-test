export function validateBlog(blog) {
  const errors = [];
  //Validate title
  if (!blog.title || blog.title.length < 6) {
    errors.push("title");
  }
  //Validate title
  if (!blog.title || blog.title.length < 0) {
    errors.push("titles");
  }
  //Validate category
  if (!blog.category || blog.category.length < 0) {
    errors.push("category");
  }
  //Validate position
  if (!blog.position || blog.position.length < 1) {
    errors.push("position");
  }
  //Validate data_pubblic
  if (!blog.data_pubblic || blog.data_pubblic.length < 0) {
    errors.push("data_pubblic");
  }
  //Validate public
  if (!blog.public || blog.public.length < 0) {
    errors.push("public");
  }

  return errors;
}
